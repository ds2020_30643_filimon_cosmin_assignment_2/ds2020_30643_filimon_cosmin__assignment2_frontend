import React from "react"
import {
    Table
} from "react-bootstrap"
const medicationPlans=null;

class AccountControl extends React.Component {
    constructor() {
        super()
        this.state = {
            patient: null,
            medicationPlans: null,
            isLoadingPatient: true,
            isLoadingMedicationPlans: true
        }
    }

    componentDidMount() {
        fetch("http://localhost:8080/patient/" + localStorage.getItem('reference'))
            .then(response => response.json())
            .then(data => {
                this.setState({
                    patient: data,
                    isLoadingPatient: false
                })
            })
            .then(() => {
                console.log(this.state.caregiver);
            })

        fetch("http://localhost:8080/medicationplan")
            .then(response => response.json())
            .then(data => {
                this.setState({
                    medicationPlans: data,
                    isLoadingMedicationPlans: false
                })
            })
            .then(() => {
                console.log(this.state.medicationPlans);
            })
    }

    getMedsAsString(id) {
        var medicationPlan=null;

        this.state.medicationPlans.find((element) => {
            if (element.id == id) {
                medicationPlan = element

            }
        })

        if(medicationPlan.medications && medicationPlan.medications.length === 0) {
            return null
        }
        return medicationPlan.medications.map((med, index) => {
            return <ol key={index}>{med.name}</ol>
        })
    }

    getMedicationPlans(medicationPlans) {
        let medPlans =null;
            medPlans = medicationPlans;
        console.log(medPlans);

        if(medPlans && medPlans.length === 0) {
            return null
        }
        return medPlans.map((medication, index) => {
            return (
                <li key={index}>
                    <div>
                        <Table striped bordered hover variant>
                            <tr>
                                <th>Intervals</th>
                                <th>Start Date</th>
                                <th>End Date</th>
                                <th>Medications</th>
                            </tr>
                            <tbody>
                            <tr>
                        <td>{medication.intakeIntervals}</td>
                        <td>{medication.startDate}</td>
                        <td>{medication.endDate}</td>
                        <td>{this.getMedsAsString(medication.id)}</td>
                            </tr>
                            </tbody>
                        </Table>
                    </div>
                </li>
            )
        })
    }

    getPatients() {
        if(this.state.caregiver.patients && this.state.caregiver.patients.length === 0) {
            return null
        }
        return this.state.caregiver.patients.map((patient, index) =>{
            return <li key={index}>{patient.name}
                <ol>{this.getMedicationPlans(patient.medicationPlans)}</ol>
            </li>
        })
    }

    render() {
        if(this.state.isLoadingPatient === true || this.state.isLoadingMedicationPlans === true) {
            return (
                <div>
                    <h1>Is loading...</h1>
                </div>
            )
        }
        return (
            <div>
                <h2>Patient Data:</h2>
                <Table striped bordered hover variant>
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Birthdate</th>
                        <th>Address</th>
                        <th>Gender</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>{this.state.patient.name}</td>
                        <td>{this.state.patient.birthdate}</td>
                        <td>{this.state.patient.address}</td>
                        <td>{this.state.patient.gender}</td>
                    </tr>
                </tbody>
                </Table>
                <h3>Medication Plans:</h3>
                 <ol>{this.getMedicationPlans(this.state.patient.medicationPlans)}</ol>
            </div>
        // <div>
        //     <Table striped bordered hover variant>
        //         <tbody>
        //         <tr>{this.renderTableHeader()}</tr>
        //         {this.renderTableData()}
        //         </tbody>
        //     </Table>
        // </div>
        )
    }
}

export default AccountControl