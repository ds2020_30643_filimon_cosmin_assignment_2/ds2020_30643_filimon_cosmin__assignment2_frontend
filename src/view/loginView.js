import React from "react"
import {
    Form, Button, Col
} from "react-bootstrap"
import Axios from "axios"
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect
} from "react-router-dom";
import {
    Navbar,
    Nav
} from "react-bootstrap"
import DoctorView from "./doctorView";
import BackgroundImg from '../commons/images/future-medicine.jpg';

import {Container, Jumbotron} from 'reactstrap';

const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "1920px",
    backgroundImage: `url(${BackgroundImg})`
};
const textStyle = {color: 'white', };
class LoginView extends React.Component {
    constructor() {
        super()
        this.state = {
            user: "",
            id: 0,
            role: "",
            username: "",
            password: "",
            canLeave: false,
            redirectTo: "/"
        }
        this.handleInputChange = this.handleInputChange.bind(this)
        this.login = this.login.bind(this)
    }

    async login() {
        await Axios.get(
            'http://localhost:8080/userlogin', {
                params: {
                    username: this.state.username,
                    password: this.state.password
                }
            }).then(response => {
            this.setState({
                canLeave: true,
                user: response.data
            }, () =>
            {
                localStorage.setItem("role", this.state.user.role);
                localStorage.setItem("reference", this.state.user.reference)
                if(this.state.user.role === "doctor") {
                    this.setState({
                        redirectTo: "/doctorview"
                    })
                } else if(this.state.user.role === "patient") {
                    this.setState({
                        redirectTo: "/patientview"
                    })
                }
                else if(this.state.user.role === "caregiver") {
                    this.setState({
                        redirectTo: "/caregiverview"
                    })
                }
            })
        })
    }

    handleInputChange(event) {
        const { name, value } = event.target
        console.log(name + " " + value);

        this.setState({
            [name]: value
        })
    }

    render() {
        return (
            <div>
                <Jumbotron fluid style={backgroundStyle}>
                    <Container fluid>
                        <h1 className="display-3" style={textStyle}>Integrated Medical Monitoring Platform for Home-care assistance</h1>
                        <p className="lead" style={textStyle}> <b>Enabling real time monitoring of patients, remote-assisted care services and
                            smart intake mechanism for prescribed medication.</b> </p>
                        <hr className="my-2"/>
                        <p  style={textStyle}> <b>This assignment represents the first module of the distributed software system "Integrated
                            Medical Monitoring Platform for Home-care assistance that represents the final project
                            for the Distributed Systems course. </b> </p>
                {this.state.canLeave && <Redirect to={this.state.redirectTo}/>}
                <Route>

                    <div style={{ width: 300, padding: 20 }}>

                        <h2 style={textStyle}>Login</h2>

                        <Form>
                            <h5 style={textStyle}>Username:</h5>
                            <Form.Group controlId="username" bsSize="large">
                            <Form.Control
                                name="username"
                                value={this.state.username}
                                placeholder="Username"
                                onChange={this.handleInputChange}
                            />
                        </Form.Group>
                            <h5 style={textStyle}>Password</h5>
                            <Form.Group controlId="password" bsSize="large">
                            <Form.Control
                                name="password"
                                value={this.state.password}
                                placeholder="Password"
                                type="password"
                                onChange={this.handleInputChange}
                            />
                            </Form.Group>
                            <Button block bsSize="large" variant="success" onClick={this.login}>Login</Button>
                        </Form>

                    </div>
                </Route>

                        <p className="lead">
                            <Button color="primary" onClick={() => window.open('http://coned.utcluj.ro/~salomie/DS_Lic/')}>Learn
                                More</Button>
                        </p>
                    </Container>
                </Jumbotron>
            </div>
        )
    }

}

export default LoginView