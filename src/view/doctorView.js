import React from "react"
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect
} from "react-router-dom";
import {
    Navbar,
    Nav
} from "react-bootstrap"
import logo from '../commons/images/icon.png';
import PatientControl from "../doctor/patient/patient-control";
import CaregiverControl from "../doctor/caregiver/caregiver-control";
import MedicationControl from "../doctor/medication/medication-control";
import MedicationPlanControl from "../doctor/medicationPlan/medicationPlan-control";
import BackgroundImg from '../commons/images/back-img.jpg';
import {Button, Container, Jumbotron} from 'reactstrap';
const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "900px",
    backgroundImage: `url(${BackgroundImg})`
};

const textStyle = {color: 'white', };

class DoctorView extends React.Component {

    render() {
        return (localStorage.getItem('role') === "doctor") ? (<div>
                <div>
                    <Jumbotron fluid style={backgroundStyle}>
                        <Container fluid>
                    <Router>
                        <div>
                            <Navbar fixed="top" bg="dark" variant="dark">
                                <Navbar.Brand href="/doctorview">
                                    <img src={logo} width={"50"}
                                         height={"35"} />
                                </Navbar.Brand>
                                <Nav className="mr-auto">
                                    <Nav.Link href="/doctorview/patients">Patients</Nav.Link>
                                    <Nav.Link href="/doctorview/caregivers">Caregivers</Nav.Link>
                                    <Nav.Link href="/doctorview/medications">Medications</Nav.Link>
                                    <Nav.Link href="/doctorview/medicationPlan">MedicationPlan</Nav.Link>
                                    <Nav.Link href="/">Log out</Nav.Link>
                                </Nav>

                            </Navbar>

                            <Switch>
                                <Route path="/doctorview/patients">
                                    <PatientControl />
                                </Route>
                                <Route path="/doctorview/caregivers">
                                    <CaregiverControl />
                                </Route>
                                <Route path="/doctorview/medications">
                                    <MedicationControl />
                                </Route>
                                <Route path="/doctorview/medicationPlan">
                                    <MedicationPlanControl />
                                </Route>
                                <h1 className="display-3" style={textStyle}>Integrated Medical Monitoring Platform for Home-care assistance</h1>

                            </Switch>
                        </div>
                    </Router>
                        </Container>
                    </Jumbotron>
                </div>
            </div>)
            : (<div>Access denied</div>);
    }

}

export default DoctorView