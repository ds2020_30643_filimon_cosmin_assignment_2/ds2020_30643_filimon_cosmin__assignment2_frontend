import React from "react"
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";
import {
    Navbar,
    Nav
} from "react-bootstrap"

import AccountControl from "../patient/accountControl";
import BackgroundImg from '../commons/images/back-img.jpg';

import {Container,Jumbotron} from 'reactstrap';
import logo from "../commons/images/icon.png";

const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "900px",
    backgroundImage: `url(${BackgroundImg})`
};
const textStyle = {color: 'white', };
class PatientView extends React.Component {

    render() {
        return (localStorage.getItem('role') === "patient") ? (
                <div>
                    <Jumbotron fluid style={backgroundStyle}>
                        <Container fluid>
                    <Router>
                        <div>
                            <Navbar fixed="top" bg="dark" variant="dark">
                                <Navbar.Brand href="/patientview">
                                    <img src={logo} width={"50"}
                                         height={"35"} />
                                </Navbar.Brand>

                                <Nav className="mr-auto">
                                    <Nav.Link href="/patientview/account">Patient Details</Nav.Link>
                                    <Nav.Link href="/">Log out</Nav.Link>
                                </Nav>

                            </Navbar>

                            <Switch>
                                <Route path="/patientview/account">
                                    <AccountControl />
                                </Route>
                            </Switch>
                            <h1 className="display-3" style={textStyle}>Integrated Medical Monitoring Platform for Home-care assistance</h1>
                            <p className="lead" style={textStyle}> <b>Enabling real time monitoring of patients, remote-assisted care services and
                                smart intake mechanism for prescribed medication.</b> </p>

                        </div>
                    </Router>
                        </Container>
                    </Jumbotron>
                </div>)
    :(<div>Log in as a patient to access this page</div>)
    }

}

export default PatientView