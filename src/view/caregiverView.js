import React from "react"
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";
import {
    Navbar,
    Nav
} from "react-bootstrap"
import PatientControl from "../caregiver/patientControl";
import BackgroundImg from '../commons/images/back-img.jpg';

import {Container,Jumbotron} from 'reactstrap';
import logo from "../commons/images/icon.png";

const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "900px",
    backgroundImage: `url(${BackgroundImg})`
};
const textStyle = {color: 'white', };
class CaregiverView extends React.Component {

    render() {

        return (localStorage.getItem('role') === "caregiver") ? (
                <div>
                    <Jumbotron fluid style={backgroundStyle}>
                        <Container fluid>
                    <Router>
                        <div>
                            <Navbar fixed="top" bg="dark" variant="dark">
                                <Navbar.Brand href="/caregiverview">
                                    <img src={logo} width={"50"}
                                         height={"35"} />
                                </Navbar.Brand>

                                <Nav className="mr-auto">
                                    <Nav.Link href="/caregiverview/patients">Patients Data</Nav.Link>
                                    <Nav.Link href="/">Log out</Nav.Link>
                                </Nav>

                            </Navbar>

                            <Switch>
                                <Route path="/caregiverview/patients">
                                    <PatientControl />
                                </Route>
                            </Switch>
                            <h1 className="display-3" style={textStyle}>Integrated Medical Monitoring Platform for Home-care assistance</h1>
                            <p className="lead" style={textStyle}> <b>Enabling real time monitoring of patients, remote-assisted care services and
                                smart intake mechanism for prescribed medication.</b> </p>
                        </div>
                    </Router>
                        </Container>
                    </Jumbotron>
                </div>) :
            (<div>Log in as a caregiver to access this page</div>)
    }

}

export default CaregiverView