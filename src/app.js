import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";
import {
    Navbar,
    Nav
} from "react-bootstrap"

import DoctorView from "./view/doctorView";
import CaregiverView from "./view/caregiverView";
import PatientView from "./view/patientView";
import LoginView from "./view/loginView";


class App extends React.Component {

    render() {

        return (
            <div>
                <Router>
                    <Switch>
                        <Route exact path="/">
                            <LoginView />
                        </Route>
                        <Route path="/loginView">
                            <LoginView />
                        </Route>
                        <Route path="/doctorView">
                            <DoctorView />
                        </Route>
                        <Route path="/caregiverView">
                            <CaregiverView />
                        </Route>
                        <Route path="/patientView">
                            <PatientView />
                        </Route>
                    </Switch>
                </Router>
            </div>

        )
    }

}



export default App
