import React from "react"

import {
    Table
} from "react-bootstrap"

class PatientControl extends React.Component {
    ws = new WebSocket('ws://localhost:9000');
    constructor() {
        super()
        this.state = {
            caregiver: null,
            medicationPlans: [],
            isLoadingCaregiver: true,
            isLoadingMedicationPlans: true
        }
    }

    componentDidMount() {
        fetch("http://localhost:8080/caregiver/" + localStorage.getItem('reference'))
            .then(response => response.json())
            .then(data => {
                this.setState({
                    caregiver: data,
                    isLoadingCaregiver: false
                })
            })
            .then(() => {
                console.log(this.state.caregiver);
            })

        fetch("http://localhost:8080/medicationplan")
            .then(response => response.json())
            .then(data => {
                this.setState({
                    medicationPlans: data,
                    isLoadingMedicationPlans: false
                })
            })
            .then(() => {
                console.log(this.state.medicationPlans);
            })
        this.ws.onopen = () => {
            // on connecting, do nothing but log it to the console
            console.log('connected')
        }

        this.ws.onmessage = evt => {
            // listen to data sent from the websocket server
            const message = JSON.parse(evt.data)
            //this.setState({ dataFromServer: message })
            console.log(message)
            alert(message.message)
        }
        this.ws.onclose = () => {
            console.log('disconnected')
            // automatically try to reconnect on connection loss

        }
    }

    getMedsAsString(id) {
        var medicationPlan
        this.state.medicationPlans.find((element) => {
            if (element.id == id) {
                medicationPlan = element
            }
        })

        if(medicationPlan.medications && medicationPlan.medications.length === 0) {
            return null
        }
        return medicationPlan.medications.map((med, index) => {
            return <ol key={index}>{med.name}</ol>
        })
    }

    getMedicationPlans(medicationPlans) {
        console.log(medicationPlans);

        if(medicationPlans && medicationPlans.length === 0) {
            return null
        }
        return medicationPlans.map((med, index) => {
            return (
                // <li key={index}>
                //     <div>
                //         <ul>Meds: {this.getMedsAsString(med.id)}</ul>
                //         <p>intakeIntervals: {med.intakeIntervals}</p>
                //         <p>startDate: {med.startDate}</p>
                //         <p>endDate: {med.endDate}</p>
                //     </div>
                // </li>
            <Table striped bordered hover variant>
                <thead>
                <tr>
                    <th>Medications</th>
                    <th>Intake Intervals</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{this.getMedsAsString(med.id)}</td>
                    <td>{med.intakeIntervals}</td>
                    <td>{med.startDate}</td>
                    <td>{med.endDate}</td>
                </tr>
                </tbody>
            </Table>
            )
        })
    }

    getPatients() {
        if(this.state.caregiver.patients && this.state.caregiver.patients.length === 0) {
            return null
        }
        return this.state.caregiver.patients.map((patient, index) =>{
            return <li key={index}>
                <h3>{patient.name}</h3>
                <ol>{this.getMedicationPlans(patient.medicationPlans)}</ol>
            </li>
        })
    }

    render() {
        if(this.state.isLoadingCaregiver === true || this.state.isLoadingMedicationPlans === true) {
            return (
                <div>
                    <h1>Loading...</h1>
                </div>
            )
        }
        return (
            <div>
                <ol>{this.getPatients()}</ol>
            </div>
        )

    }
}

export default PatientControl