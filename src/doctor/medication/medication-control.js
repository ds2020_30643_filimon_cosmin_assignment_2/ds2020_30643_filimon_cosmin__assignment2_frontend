import React from "react"

import MedicationTable from "./medication-table";
import MedicationForm from "./medication-form";

class MedicationControl extends React.Component {
    constructor() {
        super()
        this.state = {
            medications: [],
            isLoading: true
        }
    }

    componentDidMount() {
        fetch("http://localhost:8080/medication")
            .then(response => response.json())
            .then(data => {
                this.setState({
                    medications: data,
                    isLoading: false
                })
            })
    }

    render() {
        if(this.state.isLoading === true) {
            return (
                <div>
                    <h1>Is loading...</h1>
                </div>
            )
        }
        return (
            <div>
                <MedicationTable medications={this.state.medications}/>
                <MedicationForm medications={this.state.medications}/>
            </div>
        )
    }
}

export default MedicationControl