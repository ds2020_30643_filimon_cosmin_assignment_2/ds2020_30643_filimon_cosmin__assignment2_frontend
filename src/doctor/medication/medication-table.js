import React from 'react'

import {
    Table
} from "react-bootstrap"

class MedicationTable extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            medications: this.props.medications
        }
    }

    renderTableHeader() {
        if(this.state.medications && this.state.medications.length === 0) {
            return (
                null
            )
        }
        let header = Object.keys(this.state.medications[0])
        return header.map((key, index) => {
            return <th key={index}>{key.toUpperCase()}</th>
        })
    }

    renderTableData() {
        return this.state.medications.map((medication, index) => {
            const { id, name, sideEffects, dosage } = medication //destructuring
            return (
                <tr key={id}>
                    <td>{id}</td>
                    <td>{name}</td>
                    <td>{sideEffects}</td>
                    <td>{dosage}</td>
                </tr>
            )
        })
    }

    render() {

        return (
            <div>
                <Table striped bordered hover variant>
                    <tbody>
                    <tr>{this.renderTableHeader()}</tr>
                    {this.renderTableData()}
                    </tbody>
                </Table>
            </div>
        )
    }
}

export default MedicationTable