import React from "react"
import {
    Form, Button, Col
} from "react-bootstrap"
import Axios from "axios"

class MedicationForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            id: 0,
            name: "",
            dosage: "",
            sideeffects: ""
        }
        this.handleInputChange = this.handleInputChange.bind(this)
        this.insertMedication = this.insertMedication.bind(this)
        this.updateMedication = this.updateMedication.bind(this)
        this.deleteMedication = this.deleteMedication.bind(this)
    }

    /* insert into database */
    async insertMedication() {
        await Axios.post(
            'http://localhost:8080/medication',
            {
                name: this.state.name,
                dosage: this.state.dosage,
                sideEffects: this.state.sideeffects
            }
        ).then(function (response) {
            console.log(response);

        })
            .catch(function (error) {
                console.log(error);

            })
            .then(() => {
                window.location.reload()
            })
    }

    /* update medication */
    async updateMedication() {
        await Axios.put(
            'http://localhost:8080/medication',
            {
                id: this.state.id,
                name: this.state.name,
                dosage: this.state.dosage,
                sideEffects: this.state.sideeffects
            }
        ).then(function (response) {
            console.log(response);

        })
            .catch(function (error) {
                console.log(error);

            })
            .then(() => {
                window.location.reload()
            })
    }

    handleInputChange(event) {
        const {name, value} = event.target
        this.setState({
            [name]: value
        })
    }

    /* delete medication */
    async deleteMedication() {
        await Axios.delete(
            'http://localhost:8080/medication', {data: {id: this.state.id}}
        ).then(function (response) {
            console.log(response)

        })
            .catch(function (error) {
                console.log(error)
            })
            .then(() => {
                window.location.reload()
            })
    }

    handleInputChange(event) {
        const {name, value} = event.target
        this.setState({
            [name]: value
        })
    }

    render() {
        return (
            <div style={{width: 500, padding: 30}}>
                <Form>
                    <Form.Group>
                        <Form.Label>Name:</Form.Label>
                        <Form.Control
                            name="name"
                            value={this.state.name}
                            placeholder="Name?..."
                            onChange={this.handleInputChange}
                        />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Dosage:</Form.Label>
                        <Form.Control
                            name="dosage"
                            value={this.state.dosage}
                            placeholder="Dosage?..."
                            onChange={this.handleInputChange}
                        />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Side effects:</Form.Label>
                        <Form.Control
                            name="sideeffects"
                            value={this.state.sideeffects}
                            placeholder="Side effects?..."
                            onChange={this.handleInputChange}
                        />
                    </Form.Group>

                    <Form.Row>
                        <Col md="4">
                            <Button variant="primary" onClick={this.insertMedication}>Add Medication</Button>
                        </Col>
                        <Col md="3">
                            <Button variant="primary" onClick={this.updateMedication}>Update</Button>
                        </Col>
                        <Col md="2">
                            <Form.Control
                                name="id"
                                value={this.state.id}
                                placeholder="ID"
                                onChange={this.handleInputChange}
                            />
                        </Col>
                        <Col md="3">
                            <Button variant="primary" onClick={this.deleteMedication}>Delete</Button>
                        </Col>

                    </Form.Row>
                </Form>
            </div>
        )
    }
}

export default MedicationForm