import React from 'react'

import {
    Table
} from "react-bootstrap"

class CaregiverTable extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            caregivers: this.props.caregivers
        }
    }

    renderTableHeader() {
        if(this.state.caregivers && this.state.caregivers.length === 0) {
            return null
        }
        let header = Object.keys(this.state.caregivers[0])
        return header.map((key, index) => {
            return <th key={index}>{key.toUpperCase()}</th>
        })
    }

    renderTableData() {
        return this.state.caregivers.map((caregiver, index) => {
            const { id, name, birthdate, gender, address } = caregiver //destructuring
            return (
                <tr key={id}>
                    <td>{id}</td>
                    <td>{name}</td>
                    <td>{birthdate}</td>
                    <td>{gender}</td>
                    <td>{address}</td>
                </tr>
            )
        })
    }

    render() {

        return (
            <div>
                <Table striped bordered hover variant>
                    <tbody>
                    <tr>{this.renderTableHeader()}</tr>
                    {this.renderTableData()}
                    </tbody>
                </Table>
            </div>
        )
    }
}

export default CaregiverTable