import React from "react"

import CaregiverTable from "./caregiver-table";
import CaregiverForm from "./caregiver-form";

class CaregiverControl extends React.Component {
    constructor() {
        super()
        this.state = {
            caregivers: [],
            isLoading: true
        }
    }

    componentDidMount() {
        fetch("http://localhost:8080/caregiver")
            .then(response => response.json())
            .then(data => {
                this.setState({
                    caregivers: data,
                    isLoading: false
                })
            })
    }

    render() {
        if(this.state.isLoading === true) {
            return (
                <div>
                    <h1>Is loading...</h1>
                </div>
            )
        }
        return (
            <div>
                <CaregiverTable caregivers={this.state.caregivers}/>
                <CaregiverForm caregivers={this.state.caregivers}/>
            </div>
        )
    }
}

export default CaregiverControl