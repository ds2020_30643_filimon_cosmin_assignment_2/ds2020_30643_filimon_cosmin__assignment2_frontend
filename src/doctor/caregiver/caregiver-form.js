import React from "react"
import {
    Form, Button, Col
} from "react-bootstrap"
import Axios from "axios"

class CaregiverForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            id: 0,
            name: "",
            birthdate: "",
            gender: "",
            address: ""
        }
        this.handleInputChange = this.handleInputChange.bind(this)
        this.insertCaregiver = this.insertCaregiver.bind(this)
        this.updateCaregiver = this.updateCaregiver.bind(this)
        this.deleteCaregiver = this.deleteCaregiver.bind(this)
    }

    /* insert into database */
    async insertCaregiver() {
        await Axios.post(
            'http://localhost:8080/caregiver',
            {
                name: this.state.name,
                birthdate: this.state.birthdate,
                gender: this.state.gender,
                address: this.state.address
            }
        ).then(function (response) {
            console.log(response);

        })
            .catch(function (error) {
                console.log(error);

            })
            .then(() => {
                window.location.reload()
            })
    }

    /* update caregiver */
    async updateCaregiver() {
        await Axios.put(
            'http://localhost:8080/caregiver',
            {
                id: this.state.id,
                name: this.state.name,
                birthdate: this.state.birthdate,
                gender: this.state.gender,
                address: this.state.address
            }
        ).then(function (response) {
            console.log(response);

        })
            .catch(function (error) {
                console.log(error);

            })
            .then(() => {
                window.location.reload()
            })
    }

    handleInputChange(event) {
        const {name, value} = event.target
        this.setState({
            [name]: value
        })
    }

    /* delete caregiver */
    async deleteCaregiver() {
        await Axios.delete(
            'http://localhost:8080/caregiver', {data: {id: this.state.id}}
        ).then(function (response) {
            console.log(response)

        })
            .catch(function (error) {
                console.log(error)
            })
            .then(() => {
                window.location.reload()
            })
    }

    handleInputChange(event) {
        const {name, value} = event.target
        this.setState({
            [name]: value
        })
    }

    render() {
        return (
            <div style={{width: 500, padding: 30}}>
                <Form>
                    <Form.Group>
                        <Form.Label>Name:</Form.Label>
                        <Form.Control
                            name="name"
                            value={this.state.name}
                            placeholder="Name?..."
                            onChange={this.handleInputChange}
                        />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Birthdate:</Form.Label>
                        <Form.Control
                            name="birthdate"
                            value={this.state.birthdate}
                            placeholder="Birth date?..."
                            onChange={this.handleInputChange}
                        />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Gender:</Form.Label>
                        <Form.Control
                            name="gender"
                            value={this.state.gender}
                            placeholder="Gender?..."
                            onChange={this.handleInputChange}
                        />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Address:</Form.Label>
                        <Form.Control
                            name="address"
                            value={this.state.address}
                            placeholder="Address? (ex: Cluj,Mures,..)"
                            onChange={this.handleInputChange}
                        />
                    </Form.Group>

                    <Form.Row>
                        <Col md="4">
                            <Button variant="primary" onClick={this.insertCaregiver}>Add Caregiver</Button>
                        </Col>
                        <Col md="3">
                            <Button variant="primary" onClick={this.updateCaregiver}>Update</Button>
                        </Col>
                        <Col md="2">
                            <Form.Control
                                name="id"
                                value={this.state.id}
                                placeholder="ID"
                                onChange={this.handleInputChange}
                            />
                        </Col>
                        <Col md="3">
                            <Button variant="primary" onClick={this.deleteCaregiver}>Delete</Button>
                        </Col>

                    </Form.Row>
                </Form>
            </div>
        )
    }
}

export default CaregiverForm