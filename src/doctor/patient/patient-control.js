import React from "react"

import PatientTable from "./patient-table";
import PatientForm from "./patient-form";

class PatientControl extends React.Component {
    constructor() {
        super()
        this.state = {
            patients: [],
            caregivers: [],
            isLoadingPatients: true,
            isLoadingCaregivers: true
        }
    }

    componentDidMount() {
        fetch("http://localhost:8080/patient")
            .then(response => response.json())
            .then(data => {
                this.setState({
                    patients: data,
                    isLoadingPatients: false
                })
            })
        fetch("http://localhost:8080/caregiver")
            .then(response => response.json())
            .then(data => {
                this.setState({
                    caregivers: data,
                    isLoadingCaregivers: false
                })
            })
    }

    render() {
        if(this.state.isLoadingPatients === true || this.state.isLoadingCaregivers === true) {
            return (
                <div>
                    <h1>Is loading...</h1>
                </div>
            )
        }
        return (
            <div>
                <PatientTable patients={this.state.patients}/>
                <PatientForm caregivers={this.state.caregivers}/>
            </div>
        )
    }
}

export default PatientControl