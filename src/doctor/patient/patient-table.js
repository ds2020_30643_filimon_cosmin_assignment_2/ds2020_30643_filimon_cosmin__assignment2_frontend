import React from 'react'

import {
    Table
} from "react-bootstrap"

class PatientTable extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            patients: this.props.patients
        }
    }

    renderTableHeader() {
        if(this.state.patients && this.state.patients.length === 0) {
            return null
        }
        let header = Object.keys(this.state.patients[0])
        return header.map((key, index) => {
            return <th key={index}>{key.toUpperCase()}</th>
        })
    }

    renderCaregiver(caregiver) {
        if(caregiver === null) {
            return "NOT ASSIGNED"
        }
        return caregiver.name + " " + caregiver.id
    }

    renderTableData() {
        console.log(this.state.patients);

        return this.state.patients.map((patient, index) => {
            const { id, name, birthdate, gender, address } = patient
            var caregiver
            if(patient.caregiver === null) {
                caregiver = "NOT ASSIGNED"
            }
            else {
                caregiver = patient.caregiver.name + " (id=" + patient.caregiver.id + ")"
            }

            return (
                <tr key={id}>
                    <td>{id}</td>
                    <td>{name}</td>
                    <td>{birthdate}</td>
                    <td>{gender}</td>
                    <td>{address}</td>
                    <td>{caregiver}</td>
                </tr>
            )
        })
    }

    render() {

        return (
            <div>
                <Table striped bordered hover variant>
                    <tbody>
                    <tr>{this.renderTableHeader()}</tr>
                    {this.renderTableData()}
                    </tbody>
                </Table>
            </div>
        )
    }
}

export default PatientTable