import React from "react"
import {
    Form, Button, Col
} from "react-bootstrap"
import Axios from "axios"

class PatientForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            caregivers: this.props.caregivers,
            id: 0,
            name: "",
            birthdate: "",
            gender: "",
            address: "",
            caregiver: 0
        }
        this.handleInputChange = this.handleInputChange.bind(this)
        this.insertPatient = this.insertPatient.bind(this)
        this.updatePatient = this.updatePatient.bind(this)
        this.deletePatient = this.deletePatient.bind(this)
    }

    findArrayCaregiverById(array, id) {
        return array.find((element) => {
            if (element.id == id) {
                return element
            }
        })
    }

    /* insert into database */
    async insertPatient() {
        var element = this.findArrayCaregiverById(this.state.caregivers, this.state.caregiver)
        await Axios.post(
            'http://localhost:8080/patient',
            {
                name: this.state.name,
                birthdate: this.state.birthdate,
                gender: this.state.gender,
                address: this.state.address,
                caregiver: element
            }
        ).then(function (response) {
            console.log(response);

        })
            .catch(function (error) {
                console.log(error);

            })
            .then(() => {
                window.location.reload()
            })


    }

    /* update patient */
    async updatePatient() {
        var element = this.findArrayCaregiverById(this.state.caregivers, this.state.caregiver)
        await Axios.put(
            'http://localhost:8080/patient',
            {
                id: this.state.id,
                name: this.state.name,
                birthdate: this.state.birthdate,
                gender: this.state.gender,
                address: this.state.address,
                caregiver: element
            }
        ).then(function (response) {
            console.log(response);

        })
            .catch(function (error) {
                console.log(error);

            })
            .then(() => {
                window.location.reload()
            })
    }

    handleInputChange(event) {
        const {name, value} = event.target
        this.setState({
            [name]: value
        })
    }

    /* delete patient */
    async deletePatient() {
        await Axios.delete(
            'http://localhost:8080/patient', {data: {id: this.state.id}}
        ).then(function (response) {
            console.log(response)

        })
            .catch(function (error) {
                console.log(error)
            })
            .then(() => {
                window.location.reload()
            })
    }

    handleInputChange(event) {
        const {name, value} = event.target
        this.setState({
            [name]: value
        })
    }

    render() {
        let options = this.state.caregivers.map((data) =>
            <option key={data.id} value={data.id}>
                {data.name}
            </option>
        )

        return (
            <div style={{width: 500, padding: 30}}>
                <Form>
                    <Form.Group>
                        <Form.Label>Name:</Form.Label>
                        <Form.Control
                            name="name"
                            value={this.state.name}
                            placeholder="Name?..."
                            onChange={this.handleInputChange}
                        />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Birthdate:</Form.Label>
                        <Form.Control
                            name="birthdate"
                            value={this.state.birthdate}
                            placeholder="Birth date?..."
                            onChange={this.handleInputChange}
                        />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Gender:</Form.Label>
                        <Form.Control
                            name="gender"
                            value={this.state.gender}
                            placeholder="Gender?..."
                            onChange={this.handleInputChange}
                        />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Address:</Form.Label>
                        <Form.Control
                            name="address"
                            value={this.state.address}
                            placeholder="Address? (ex: Cluj,Mures,..)"
                            onChange={this.handleInputChange}
                        />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Caregiver:</Form.Label>
                        <Form.Control
                            as="select"
                            name="caregiver"
                            value={this.state.caregiver}
                            onChange={this.handleInputChange}>
                            <option key="0" value="0">Select caregiver</option>
                            {options}
                        </Form.Control>
                    </Form.Group>

                    <Form.Row>
                        <Col md="4">
                            <Button variant="primary" onClick={this.insertPatient}>Add Patient</Button>
                        </Col>
                        <Col md="3">
                            <Button variant="primary" onClick={this.updatePatient}>Update</Button>
                        </Col>
                        <Col md="2">
                            <Form.Control
                                name="id"
                                value={this.state.id}
                                placeholder="ID"
                                onChange={this.handleInputChange}
                            />
                        </Col>
                        <Col md="3">
                            <Button variant="primary" onClick={this.deletePatient}>Delete</Button>
                        </Col>
                    </Form.Row>
                </Form>
            </div>
        )
    }

}

export default PatientForm