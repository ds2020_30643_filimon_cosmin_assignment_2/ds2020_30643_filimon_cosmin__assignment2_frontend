import React from "react"
import MedicationPlanForm from "./medicationPlan-form"

class MedicationPlanControl extends React.Component {
    constructor() {
        super()
        this.state = {
            patients: [],
            medications: [],
            isLoadingPatients: true,
            isLoadingMedications: true
        }
    }

    componentDidMount() {
        fetch("http://localhost:8080/medication")
            .then(response => response.json())
            .then(data => {
                this.setState({
                    medications: data,
                    isLoadingMedications: false
                })
            })
        fetch("http://localhost:8080/patient")
            .then(response => response.json())
            .then(data => {
                this.setState({
                    patients: data,
                    isLoadingPatients: false
                })
            })
    }

    render() {
        if(this.state.isLoadingPatients === true || this.state.isLoadingMedications === true) {
            return (
                <div>
                    <h1>Is loading...</h1>
                </div>
            )
        }
        return (
            <div>
                <MedicationPlanForm medications={this.state.medications} patients = {this.state.patients}/>
            </div>
        )
    }
}

export default MedicationPlanControl