import React from "react"
import {
    Form, Button, Col
} from "react-bootstrap"
import Axios from "axios"

class MedicationPlanForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            patientList: this.props.patients,
            medicationList: this.props.medications,
            intakeIntervals: "",
            startDate: "",
            endDate: "",
            patient: 0,
            medicationSelected: 0,
            medications: []
        }
        this.handleInputChange = this.handleInputChange.bind(this)
        this.insertMedicationPlan = this.insertMedicationPlan.bind(this)
        this.addMedicationToList = this.addMedicationToList.bind(this)
    }

    findArrayElementById(array, id) {
        return array.find((element) => {
            if (element.id == id) {
                return element
            }
        })
    }

    /* insert into database */
    async insertMedicationPlan() {
        var patient = this.findArrayElementById(this.state.patientList, this.state.patient)
        console.log(this.state.medications);

        await Axios.post(
            'http://localhost:8080/medicationplan',
            {
                intakeIntervals: this.state.intakeIntervals,
                startDate: this.state.startDate,
                endDate: this.state.endDate,
                patient: patient,
                medications: this.state.medications
            }
        ).then(function (response) {
            console.log(response);

        })
            .catch(function (error) {
                console.log(error);

            })
            .then(() => {
                window.location.reload()
            })
    }

    addMedicationToList() {
        this.setState(previousState => ({
            medications: [...previousState.medications, this.findArrayElementById(this.state.medicationList, this.state.medicationSelected)]
        }))
    }

    handleInputChange(event) {
        const {name, value} = event.target
        this.setState({
            [name]: value
        })
    }

    renderTableHeader() {
        if(this.state.medications && this.state.medications.length === 0) {
            return (
                null
            )
        }
        let header = Object.keys(this.state.medications[0])
        return header.map((key, index) => {
            return <th key={index}>{key.toUpperCase()}</th>
        })
    }

    getListItems() {
        if(this.state.medications && this.state.medications.length === 0) {
            return (
                null
            )
        }
        return this.state.medications.map((med, index) =>{
            return <li key={index}>{med.name}</li>
        })
    }

    render() {
        let patientOptions = this.state.patientList.map((data) =>
            <option key={data.id} value={data.id}>
                {data.name}
            </option>
        )

        let medicationOptions = this.state.medicationList.map((data) =>
            <option key={data.id} value={data.id}>
                {data.name}
            </option>
        )

        return (
            <div style={{width: 500, padding: 30}}>
                <Form>
                    <Form.Group>
                        <Form.Label>Intake Intervals:</Form.Label>
                        <Form.Control
                            name="intakeIntervals"
                            value={this.state.intakeIntervals}
                            placeholder="Intervals?..."
                            onChange={this.handleInputChange}
                        />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Start Date:</Form.Label>
                        <Form.Control
                            name="startDate"
                            value={this.state.startDate}
                            placeholder="Start Date?..."
                            onChange={this.handleInputChange}
                        />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>End Date:</Form.Label>
                        <Form.Control
                            name="endDate"
                            value={this.state.endDate}
                            placeholder="End Date?..."
                            onChange={this.handleInputChange}
                        />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Patient:</Form.Label>
                        <Form.Control
                            as="select"
                            name="patient"
                            value={this.state.patient}
                            onChange={this.handleInputChange}>
                            <option key="0" value="0">Select patient</option>
                            {patientOptions}
                        </Form.Control>
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Medication:</Form.Label>
                        <Form.Control
                            as="select"
                            name="medicationSelected"
                            value={this.state.medicationSelected}
                            onChange={this.handleInputChange}>
                            <option key="0" value="0">Add a medication</option>
                            {medicationOptions}
                        </Form.Control>
                    </Form.Group>

                    <Form.Row>
                        <Col md="4">
                            <Button variant="primary" onClick={this.addMedicationToList}>Add Meds</Button>
                        </Col>
                        <Col md="6">
                            <Button variant="primary" onClick={this.insertMedicationPlan}>Create Medication Plan</Button>
                        </Col>
                    </Form.Row>
                </Form>

                <ul>{this.getListItems()}</ul>
            </div>
        )
    }

}

export default MedicationPlanForm